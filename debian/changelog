libgraphics-primitive-perl (0.67-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Add missing build dependency on libmodule-install-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 22:12:16 +0100

libgraphics-primitive-perl (0.67-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 13 Jun 2022 17:53:30 +0200

libgraphics-primitive-perl (0.67-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Import upstream version 0.67.
  * Update years of packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sat, 31 Oct 2015 19:02:33 +0100

libgraphics-primitive-perl (0.65-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.

 -- gregor herrmann <gregoa@debian.org>  Tue, 06 May 2014 19:31:29 +0200

libgraphics-primitive-perl (0.64-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop patches, both merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Feb 2014 23:35:51 +0100

libgraphics-primitive-perl (0.62-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Mark spelling.patch as forwarded.
  * Add new patch to add encoding directive to POD.
  * Update years of copyright.
  * Bump debhelper compatibility level to 8.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Feb 2014 20:36:35 +0100

libgraphics-primitive-perl (0.61-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * debian/copyright: Update for new upstream release.
  * Bump Standards-Version to 3.9.2 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 02 Jun 2011 18:41:24 +0200

libgraphics-primitive-perl (0.53-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Refresh copyright information
  * Standards-Version 3.9.1 (no changes)

  [ gregor herrmann ]
  * Refresh spelling.patch (offset).

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 20 Feb 2011 19:55:58 -0500

libgraphics-primitive-perl (0.51-1) unstable; urgency=low

  [ Brian Cassidy ]
  * New upstream release
  * debian/control:
    + Added myself to Uploaders
    + Remove dep on libmoosex-attributehelpers-perl
    + Update Standards-Version to 3.8.4
  * debian/copyright:
    + Added myself to copyright for debian/*

  [ gregor herrmann ]
  * Add patch to fix spelling mistakes in POD.
  * Convert to source format 3.0 (quilt).

 -- Brian Cassidy <brian.cassidy@gmail.com>  Tue, 23 Mar 2010 13:55:57 -0300

libgraphics-primitive-perl (0.49-1) unstable; urgency=low

  * Initial Release (Closes: #566898)

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 25 Jan 2010 15:53:12 -0500
